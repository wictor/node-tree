package cz.sysrq.algo;

import java.util.List;

public interface NodeTree<T> {

    void addNode(T node);
    T getById(long id);
    List<T> getNodes();
    List<T> getRoots();
    List<T> getRootsOf(T node);
    List<T> getLeaves();
    List<T> getLeavesOf(T node);
}
