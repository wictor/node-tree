package cz.sysrq.algo;

import java.util.Objects;

/**
 * Node with parent relation
 */
public class PNode {

    private final long id;
    private final String name;
    private final PNode parent;

    public PNode(long id) {
        this(id, null);
    }

    public PNode(long id, String name) {
        this(id, name, null);
    }

    public PNode(long id, String name, PNode parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PNode getParent() {
        return parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PNode pNode = (PNode) o;
        return id == pNode.id && Objects.equals(name, pNode.name) && Objects.equals(parent, pNode.parent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, parent);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PNode{").append(id);
        if (name != null) {
            sb.append(" (").append(name).append(")");
        }
        if (parent != null) {
            sb.append(" parent=").append(parent);
        }
        sb.append("}");
        return sb.toString();
    }
}
