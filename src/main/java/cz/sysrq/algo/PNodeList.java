package cz.sysrq.algo;

import java.util.*;
import java.util.stream.Collectors;

/**
 * PNodeList
 * <ul>
 *     <li>PNode = Parent-link nodes</li>
 *     <li>List = flat collection of all nodes</li>
 * </ul>
 */
public class PNodeList implements NodeTree<PNode> {

    private final List<PNode> nodes = new LinkedList<>();

    @Override
    public void addNode(PNode node) {
        nodes.add(node);
    }

    @Override
    public List<PNode> getNodes() {
        return nodes;
    }

    @Override
    public PNode getById(long id) {
        return nodes.stream().filter(n -> n.getId() == id).findFirst().orElseThrow();
    }

    @Override
    public List<PNode> getRoots() {
        return nodes.stream().filter(n -> n.getParent() == null).collect(Collectors.toList());
    }

    @Override
    public List<PNode> getRootsOf(PNode node) {
        if (node.getParent() == null) {
            return List.of(node);
        } else {
            PNode parent = getById(node.getParent().getId());
            return getRootsOf(parent);
        }
    }

    @Override
    public List<PNode> getLeaves() {
        List<Long> parentIdList = nodes.stream()
                .filter(n -> n.getParent() != null)
                .map(n -> n.getParent().getId())
                .collect(Collectors.toList());
        return nodes.stream()
                .filter(n -> !parentIdList.contains(n.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<PNode> getLeavesOf(PNode node) {
        List<PNode> children = nodes.stream()
                .filter(n -> n.getParent() != null && n.getParent().getId() == node.getId())
                .collect(Collectors.toList());
        if (children.isEmpty()) {
            return List.of(node);
        } else {
            return children.stream()
                    .map(this::getLeavesOf)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public String toString() {
        return "PNodeList{" +
                "nodes=" + nodes +
                '}';
    }
}
