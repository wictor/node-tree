package cz.sysrq.algo;

import org.junit.jupiter.api.Test;

import static cz.sysrq.algo.TestUtils.print;
import static org.junit.jupiter.api.Assertions.*;

class PNodeListTest {

    @Test
    void test1() {
        PNodeList nodeList = new PNodeList();
        nodeList.addNode(new PNode(1, "Root A"));
        nodeList.addNode(new PNode(2, "Root B"));
        nodeList.addNode(new PNode(3, "Root C"));
        nodeList.addNode(new PNode(11, "A1", new PNode(1)));
        nodeList.addNode(new PNode(12, "A2", new PNode(1)));
        nodeList.addNode(new PNode(121, "A21", new PNode(12)));
        nodeList.addNode(new PNode(1211, "A211", new PNode(121)));
        nodeList.addNode(new PNode(31, "C1", new PNode(3)));
        nodeList.addNode(new PNode(32, "C2", new PNode(3)));

        assertEquals("C2", nodeList.getById(32).getName());
        assertEquals("A211", nodeList.getById(1211).getName());

        print("Nodes: %s", nodeList.getNodes());
        print("Roots: %s", nodeList.getRoots());
        print("Root of %s: %s", nodeList.getById(32).getName(), nodeList.getRootsOf(nodeList.getById(32)));
        print("Root of %s: %s", nodeList.getById(1).getName(), nodeList.getRootsOf(nodeList.getById(1)));
        print("Leaves: %s", nodeList.getLeaves());
        print("Leaves of %s: %s", nodeList.getById(12).getName(), nodeList.getLeavesOf(nodeList.getById(12)));
        print("Leaves of %s: %s", nodeList.getById(2).getName(), nodeList.getLeavesOf(nodeList.getById(2)));
    }
}