package cz.sysrq.algo;

class TestUtils {

    /**
     * Shortcut function for printing to standard output.
     *
     * @param tml    format template
     * @param params parameters
     */
    static void print(String tml, Object... params) {
        System.out.printf(tml + "\n", params);
    }
}
